drop table if exists users;
CREATE TABLE users (
    id int UNIQUE,
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    job_title varchar(255),
    star boolean
);
INSERT into users(id, first_name, last_name, email, job_title, star) values
(1,'Terry','Medhurst','terry.medhurst@gmail.com','Corporate Operations Engineer',false),
(2,'John','Allcot','john.allcot@gmail.com','Dynamic Intranet Analyst',false),
(3,'Alan','Wattson','alan.wattson@gmail.com','National Identity Orchestrator',true),
(4,'Anna','Bishop','anna.bishop@gmail.com','Regional Integration Executive',false),
(5,'Samuel','Sharp','samuel.sharp@gmail.com','Future Resonance Architect',false);

drop table if exists products;
CREATE TABLE products (
    id int UNIQUE,
    title varchar(255),
    description varchar(255),
    price int
);
INSERT into products(id,title,description,price) values 
(1, 'iPhone 14', 'the newest iPhone', 000), 
(2, 'Chocolate Muffin', 'the best desert there is', 000),
(3, 'Philosophers stone', 'Mythic alchemical substance capable of turning base metals such as mercury into gold', 9999);

drop table if exists purchases;
CREATE TABLE purchases (
    id int,
    product int references products(id),
    buyer int references users(id),
    count int
);
INSERT into purchases(id,product,buyer,count) values 
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 2),
(4, 1, 5, 1),
(5, 2, 1, 2),
(6, 3, 1, 1),
(7, 2, 2, 5),
(8, 2, 4, 3),
(9, 2, 3, 2),
(10, 2, 3, 4);
