import express from "express";
import Knex from "knex"
import fs, { readFileSync } from "fs"
import path from "path"
import { cwd } from "process";

import router from "./api/v1/users.js"

const port = 3333;
const app = express();

const knex = Knex({
    client: "sqlite3",
    connection: {
        filename: "./db.sqlite3"
    }
})

const csvPath = path.join(cwd(), "src/data/users.csv")
const csvContent = readFileSync(csvPath).toString()
const [rawHeaders, ...rawEntries] = csvContent.split("\n")

const headers = rawHeaders.trim().split(",")

export const users = rawEntries.map(rawEntry => {
    const values = rawEntry.trim().split(",")
    const userObject = values.reduce(
        (total, value, index) => ({
            ...total, 
            [headers[index]]: value
    }), {})
    return userObject
})

app.get("/zaptic", (req, res) => {
    const file = fs.readFileSync("./public/res/zaptic_logo.jpeg")
    return res.send(file).header("Content-Type", "image/jpeg").status(200)
})
app.get("/", (req, res) => {
    res.send("Hello from Zaptic! 👋💜");
});
app.use("/api/v1/users", router)
app.use((req, res, next) => {
    return res.status(404).send("Seems like you're lost😱. Do you need some help?🕵️")
})
app.listen(port, () => {
    console.log(`Listening: http://localhost:${port}`);
});

export default app;
