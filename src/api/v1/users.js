import { json } from "express";
import { Router } from "express";
import { users } from "../../app.js";

const router = Router()
router.use((req, res, next) => {
  // These routes should return JSON
  res.header("Content-Type", "application/json")
  next()
})
router.get("/", (req, res) => {
  return res.send({ users }).status(200)
})
router.get("/:id", (req,res) => {
  console.log({ t: req.params.id })
  const foundUser = users.find(({ id }) => id === req.params.id)

  if (!foundUser) {
    return res.send("Not found").status(404)
  }

  return res.send(foundUser).status(200)
})

const verySecureAuth = (req, res, next) => {
  if (req.body?.password === "password") {
    next()
  } else {
    res.status(403).send("The password is not provided (its 'password')")
  }
}

router.post("/", json(), verySecureAuth, (req, res) => {
  try {

    const user = {
      id: (users.length + 1).toString(),
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      job_title: req.body.job_title,
      star: req.body.star
    }
    users.push(user)
    
    return res.send(user).status(201)
  } catch (error) {
    return res.send("Something went wrong").status(500)
  }
})

export default router