-- 1. write an SQL statement to get all attributes except id of products that have the word “stone” in their title
SELECT
  title, description, price
FROM products
WHERE
  title LIKE '%stone%'

-- Output
-- Philosophers stone | Mythic alchemical substance capable of turning base metals such as mercury into gold | 9999