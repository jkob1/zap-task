-- list all purchases of star users (users which have star = true) in the following format:
-- purchase id, product name, count, buyer email
SELECT
  purchases.id, products.title, count, users.email
FROM users
INNER JOIN purchases
  ON purchases.buyer = users.id
INNER JOIN products
  ON purchases.product = products.id
WHERE
  users.star = TRUE

-- 3 | iPhone 14 | 2 | alan.wattson@gmail.com
-- 9 | Chocolate Muffin | 2 | alan.wattson@gmail.com
-- 10 | Chocolate Muffin | 4 | alan.wattson@gmail.com
