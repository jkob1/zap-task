-- list all user emails who purchased at least one iPhone 14
SELECT
  users.email
FROM users
INNER JOIN purchases
  ON purchases.buyer = users.id
INNER JOIN products
  ON purchases.product = products.id
WHERE
  purchases.product = 1
GROUP BY 
  users.email 
HAVING COUNT(purchases.id) >= 1

-- OUTPUT
-- alan.wattson@gmail.com
-- john.allcot@gmail.com
-- samuel.sharp@gmail.com
-- terry.medhurst@gmail.com